
<!-- README.md is generated from README.Rmd. Please edit that file -->

# databaseDevelopmentTools

<!-- badges: start -->
<!-- badges: end -->

A collection of helper functions to aid the development of research
databases.

## installation

``` r
devtools::install_gitlab("CAPLTER/databaseDevelopmentTools")
```
