#' @title R dataframe to postgres table
#'
#' @description r_pg_table writes a dataframe as a table in postgres at the
#'  specified location. In addition, a sequence married to the id column is
#'  constructed.
#'
#' @details The location where the table and sequence are written is determined
#'  by the connection string (which must exist in the R environment) and the
#'  schema name, which is supplied to the function.
#'
#' @note r_pg_table assumes that the dataframe has an `id` column (with that
#'  name), and that the `id` column is the primary key.
#'
#' @param connection
#'  (character) Unquoted postgres connection string (typically `pg`, which is
#'  also the default)
#' @param schema
#'  (character) Quoted name of the target schema
#' @param table
#'  (character) Quoted name of the dataframe or tibble to write, which will
#'  also be the name of the table in postgres
#' @param owner
#'  (character) Quoted name of the database, schema, or table owner to which the
#'  table should belong
#'
#' @importFrom DBI dbExecute dbWriteTable dbWithTransaction dbRemoveTable
#'
#' @examples
#' \dontrun{
#'
#' r_pg_table(
#'  schema = "urbancndep",
#'  table = "cover_composition",
#'  owner = "caplter"
#' )
#'
#' }
#'
#' @export

r_pg_table <- function(
  connection = pg,
  schema,
  table,
  owner) {

  if (DBI::dbExistsTable(conn = connection, name = c(schema, table))) {

    stop("that table already exists")

  }

  table_data <- get(table)

  # notation helpers
  schema_dot_table <- paste0(schema, ".", table)
  schema_dot_sequence <- paste0(schema, ".", table, "_id_seq")

  # construct queries
  table_owner <- paste0("ALTER TABLE ", schema_dot_table, " OWNER TO ", owner, " ;")
  create_sequence <- paste0("CREATE SEQUENCE ", schema_dot_sequence, " ;")

  id_sequence <- paste0("ALTER TABLE ", schema_dot_table, " ALTER COLUMN id SET DEFAULT nextval(\'", schema_dot_sequence, "\') ;")
  id_not_null <- paste0("ALTER TABLE ", schema_dot_table, " ALTER COLUMN id SET NOT NULL ;")
  id_integer <- paste0("ALTER TABLE ", schema_dot_table, " ALTER COLUMN id TYPE integer ;")
  id_pk <- paste0("ALTER TABLE ", schema_dot_table, " ADD PRIMARY KEY(id) ;")

  sequence_owner <- paste0("ALTER SEQUENCE ", schema_dot_sequence, " OWNER TO ", owner, " ;")
  sequence_owned <- paste0("ALTER SEQUENCE ", schema_dot_sequence, " OWNED BY ", schema_dot_table, ".id ;")

  sequence_id <- paste0("SELECT setval(\'", schema_dot_sequence, "\', mx.mx) FROM (SELECT MAX(id) AS mx FROM ", schema_dot_table, ") mx ;")

  # write table
  DBI::dbWriteTable(
    conn = connection,
    name = c(schema, table),
    value = table_data,
    row.names = F
  )

  # run modifying queries
  tryCatch({

    DBI::dbWithTransaction(
      connection, {
        DBI::dbExecute(connection, table_owner)
        DBI::dbExecute(connection, create_sequence)
        DBI::dbExecute(connection, id_sequence)
        DBI::dbExecute(connection, id_not_null)
        DBI::dbExecute(connection, id_integer)
        DBI::dbExecute(connection, id_pk)
        DBI::dbExecute(connection, sequence_owner)
        DBI::dbExecute(connection, sequence_owned)
        DBI::dbExecute(connection, sequence_id)
      }
    ) # close dbWithTransaction

  }, error = function(err) {

    # remove table if error
    DBI::dbRemoveTable(conn = connection, name = c(schema, table))
    print(paste("ERROR: ", err))

  }) # close tryCatch

}
